from mod_pbxproj import XcodeProject
import sys, os

project_path = sys.argv[1]
path_to_custom_framework = sys.argv[2]
use_lib = False

if len(sys.argv) > 3 and sys.argv[3] == '-use_lib':
	use_lib = True

project = XcodeProject.Load(project_path+'/Unity-iPhone.xcodeproj/project.pbxproj')

fw = project.get_or_create_group('Frameworks')
project.add_file_if_doesnt_exist('System/Library/Frameworks/CoreText.framework', parent=fw, weak=False, tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/CoreTelephony.framework', parent=fw, weak=False, tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/CoreData.framework', parent=fw, weak=False, tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/AddressBook.framework', parent=fw, weak=False, tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/AssetsLibrary.framework', parent=fw, weak=False, tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/EventKitUI.framework', parent=fw, weak=False, tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/EventKit.framework', parent=fw, weak=False, tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/WebKit.framework', parent=fw, weak=False, tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/Social.framework', parent=fw, weak=True, tree='SDKROOT')

files_in_dir = os.listdir(path_to_custom_framework)

for f in files_in_dir:
	if f.endswith('.framework'):
		project.add_file_if_doesnt_exist(path_to_custom_framework+'/'+f, parent=fw, weak=False)
		print 'Added: '+f

if use_lib:
	project.add_file_if_doesnt_exist('usr/lib/libc++.dylib', parent=fw, weak=False, tree='SDKROOT')
	project.add_file_if_doesnt_exist('usr/lib/libz.dylib', parent=fw, weak=False, tree='SDKROOT')
	project.add_file_if_doesnt_exist('usr/lib/libsqlite3.dylib', parent=fw, weak=False, tree='SDKROOT')

project.add_other_ldflags(['-ObjC', '-fobjc-arc', '-lc++', '-lz', '-lsqlite3'])
project.save()

print 'Succees!'