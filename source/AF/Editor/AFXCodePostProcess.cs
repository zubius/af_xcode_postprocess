﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.XCodeEditor;
using System.IO;
using UnityEditor.AFUnityEditor;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Diagnostics;

public class AFXCodePostProcess {

	[PostProcessBuild(200)]
	public static void ChangeXcodePlist(BuildTarget buildTarget, string path)
	{
		if (buildTarget == BuildTarget.iPhone) {
			var customFrameworksPath = System.IO.Path.Combine(Application.dataPath, "AF/Editor/iOS");
			var scriptPath = customFrameworksPath + "/Unity2XCode.py";
			var xconffile = customFrameworksPath + "/unity.xcconfig";
			UpdatePlist(path);
			RunModPBXproj(scriptPath, path, customFrameworksPath, false); //If using XCode < 7 set true
			RunXCodeArchive(path, xconffile);
		}
	}

	public static void UpdatePlist(string path)
	{
		const string fileName = "Info.plist";
		string fullPath = Path.Combine(path, fileName);
		
		var parser = new AFPListParser(fullPath);
		parser.UpdatePList();
		parser.WriteToFile();
	}

	static void RunModPBXproj (string cmd, string pathToProject, string args, bool useLibs = false)
	{
		ProcessStartInfo start = new ProcessStartInfo ("python");
		if (useLibs) {
			args += " -use_lib";
		}
		start.Arguments = string.Format ("{0} {1} {2}", cmd, pathToProject, args);
		start.UseShellExecute = false;
		start.RedirectStandardOutput = true;

		using (Process p = Process.Start(start)) {
			using (StreamReader r = p.StandardOutput) {
				string res = r.ReadToEnd();
				UnityEngine.Debug.Log(res);
			}
		}
	}

	static void RunXCodeArchive (string pathToProject, string xconffile)
	{
		ProcessStartInfo start = new ProcessStartInfo ("xcodebuild");

		start.WorkingDirectory = pathToProject;
		start.Arguments = string.Format ("{0} {1} {2}", "-scheme Unity-iPhone", "-xcconfig "+xconffile, "archive");
		UnityEngine.Debug.Log(start.Arguments);
		start.UseShellExecute = false;
		start.RedirectStandardOutput = true;
		
		using (Process p = Process.Start(start)) {
			using (StreamReader r = p.StandardOutput) {
				string res = r.ReadToEnd();
				UnityEngine.Debug.Log(res);
			}
		}
	}
}
