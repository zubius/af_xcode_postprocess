﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEditor.AFUnityEditor
{
	public class AFPListParser
	{
		public AFPListDict xmlDict;
		private string filePath;
		
		public AFPListParser(string fullPath)
		{
			filePath = fullPath;
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.ProhibitDtd = false;
			XmlReader plistReader = XmlReader.Create(filePath, settings);
			
			XDocument doc = XDocument.Load(plistReader);
			XElement plist = doc.Element("plist");
			XElement dict = plist.Element("dict");
			xmlDict = new AFPListDict(dict);
			plistReader.Close();
		}
		
		public void UpdatePList()
		{
			if (!xmlDict.ContainsKey ("NSAppTransportSecurity")) {
				var t = new XElement ("t", new XElement ("key", "NSAllowsArbitraryLoads"), new XElement ("true"));
				xmlDict.Add ("NSAppTransportSecurity", new AFPListDict (t));
			} else if (xmlDict.ContainsKey ("NSAppTransportSecurity") && xmlDict ["NSAppTransportSecurity"].GetType () == typeof(AFPListDict) &&
			           !((AFPListDict)xmlDict ["NSAppTransportSecurity"]).ContainsKey ("NSAllowsArbitraryLoads")) {
				//				((AFPListDict)xmlDict ["NSAppTransportSecurity"]).Add ("NSAllowsArbitraryLoads", "true");
			} else {
				//				((AFPListDict)xmlDict["NSAppTransportSecurity"])["NSAllowsArbitraryLoads"] = "true";
			}

			Debug.Log("NSAppTransportSecurity - NSAllowsArbitraryLoads: "+((AFPListDict)xmlDict["NSAppTransportSecurity"])["NSAllowsArbitraryLoads"]);
			if (!xmlDict.ContainsKey ("LSApplicationQueriesSchemes")) {
				xmlDict.Add ("LSApplicationQueriesSchemes", new List<object> (new string[] {"fb", "instagram", "tumblr" ,"twitter"}));
			}
			
			if (xmlDict.ContainsKey ("UIRequiredDeviceCapabilities")) {
				var dc = (List<object>)xmlDict["UIRequiredDeviceCapabilities"];
				dc.Add((object)"gamekit");
			} else {
				xmlDict.Add ("UIRequiredDeviceCapabilities", new List<object> (new string[] {"gamekit"}));
			}

			if (xmlDict.ContainsKey ("UIRequiresFullScreen")) {
//				var dc = (List<object>)xmlDict["UIRequiredDeviceCapabilities"];
//				dc.Add((object)"gamekit");
			} else {
				xmlDict.Add ("UIRequiresFullScreen", true);
			}
			Debug.Log("UIRequiresFullScreen: "+xmlDict["UIRequiresFullScreen"]);
		}
		
		public void WriteToFile()
		{
			// Corrected header of the plist
			string publicId = "-//Apple//DTD PLIST 1.0//EN";
			string stringId = "http://www.apple.com/DTDs/PropertyList-1.0.dtd";
			string internalSubset = null;
			XDeclaration declaration = new XDeclaration("1.0", "UTF-8", null);
			XDocumentType docType = new XDocumentType("plist", publicId, stringId, internalSubset);
			
			xmlDict.Save(filePath, declaration, docType);
		}
	}
}